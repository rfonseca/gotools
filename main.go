package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"log/slog"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var (
	repositoryFile    string
	repository        map[string]struct{}
	repositoryUpdated bool

	flags struct {
		Verbose        bool
		Dry            bool
		DefaultVersion string
	}
)

func init() {
	log.SetFlags(0)
	log.SetPrefix("[GOTOOLS] ")

	userConfigDir, err := os.UserConfigDir()
	if err != nil {
		log.Fatalln("Failed to get user config directory:", err)
	}
	configDir := filepath.Join(userConfigDir, "gotools")
	err = os.MkdirAll(configDir, 0777)
	if err != nil {
		log.Fatalln("Faied to make gotools config directory:", err)
	}

	repositoryFile = filepath.Join(configDir, "repository.json")

}

func main() {
	flag.BoolVar(&flags.Verbose, "v", false, "Verbose. Prints more stuff")
	flag.BoolVar(&flags.Dry, "dry", false, "Dry run. Prints command that would be executed but don't actually execute it.")
	flag.StringVar(&flags.DefaultVersion, "version", "latest", "Defaul version if module doesn't specify one with @.")

	flag.Usage = func() {
		fmt.Printf(`%s COMMAND [MODULE]

gotools installs go modules and makes it easy to update previous installed modules.

Commands:
  install MODULE -- Installs a new module and adds it to the list.
  remove MODULE  -- Removes a module from the list but doesn't uninstall it.
  list           -- List modules installed via gotools.
  update         -- Updates modules installed via gotools.
`, os.Args[0])
	}

	flag.Parse()

	var cmd string
	if flag.NArg() == 0 {
		slog.Info("Missing command. Assuming 'update'.")
		cmd = "update"
	} else {
		cmd = flag.Arg(0)
	}

	loadRepository()
	defer persistRepository()

	switch cmd {
	case "install":
		if flag.NArg() != 2 {
			panic("missing module arg")
		}
		module := flag.Arg(1)
		if err := install(module); err != nil {
			slog.Error("Failed to install module", "module", module, "err", err)
			os.Exit(1)
		}
		addModule(module)
	case "list":
		for module := range repository {
			fmt.Println(module)
		}
	case "update":
		for module := range repository {
			if err := install(module); err != nil {
				log.Fatalln("Failed to install module", module, ":", err)
			}
		}
	case "remove":
		if flag.NArg() != 2 {
			panic("missing modulearg ")
		}
		module := flag.Arg(1)
		if _, ok := repository[module]; ok {
			log.Println("Module", module, "removed")
			delete(repository, module)
			repositoryUpdated = true
		}
	default:
		flag.Usage()
		log.Fatalln("Invalid command:", cmd)
	}
}

func loadRepository() {
	if repository == nil {
		repository = make(map[string]struct{})
	}

	_, err := os.Stat(repositoryFile)
	if os.IsNotExist(err) {
		return
	}
	if err != nil {
		panic(err)
	}

	f, err := os.Open(repositoryFile)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	dec := json.NewDecoder(f)
	err = dec.Decode(&repository)
	if err != nil {
		panic(err)
	}
}

func persistRepository() {
	if !repositoryUpdated {
		return
	}

	f, err := os.OpenFile(repositoryFile, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0660)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	enc := json.NewEncoder(f)
	enc.SetIndent("", "    ")

	err = enc.Encode(repository)
	if err != nil {
		panic(err)
	}
}

func addModule(module string) {
	if _, ok := repository[module]; !ok {
		if flags.Dry {
			slog.Info("[DRY-RUN] Added new module to config", "module", module)
		} else {
			repository[module] = struct{}{}
			repositoryUpdated = true
		}
	}
}

func install(module string) error {
	module = withVersion(module, flags.DefaultVersion)

	opts := make([]string, 0, 3)
	opts = append(opts, "install")
	if flags.Verbose {
		opts = append(opts, "-v")
	}
	opts = append(opts, module)

	cmd := exec.Command("go", opts...)
	cmd.Env = append(cmd.Env, os.Environ()...)
	cmd.Env = append(cmd.Env, "GO111MODULE=on")
	cmd.Stdin = os.Stdin
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout

	if flags.Dry {
		slog.Info("[DRY-RUN] "+cmd.String(), "env", cmd.Env)
		return nil
	}
	return cmd.Run()
}

func withVersion(module, version string) string {
	if idx := strings.Index(module, "@"); idx == -1 {
		return module + "@" + version
	} else if module[idx+1:] != version {
		slog.Warn("Module is set to a different version of what was expected. Module's version will be used but you may want to check that.", "module", module, "expected", version, "got", module[idx+1:])
	}

	return module
}
